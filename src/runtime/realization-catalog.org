#+TITLE: Realization Catalog
#+AUTHOR: VLEAD
#+DATE: [2018-11-29 Thu]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document captures the realization catalog of bubble sort
Experiment.

* Realization Catalog

#+NAME: realization-catalog
#+BEGIN_SRC js

var realizationCatalog = {
	"Trees&graphs Visualization":{
		"resource_type": "Image",
		"url":"/build/code/static/img/graphtypes.png",
		"width":"100%",
		"height":"80%"
	},

	"Step-wise Representation of finding topological sort through DFS": {
		"resource_type": "Image",
		"url": "/build/code/static/img/topo-steps/topo1.png",
		"height": "100%",
		"width": "100%"
	},

	"Step-wise Representation of Kahn's Algorithm": {
		"resource_type": "Image",
		"url": "/build/code/static/img/topo-kahns/topo3.png",
		"height": "100%",
		"width": "100%"
	},

	"Example of Topological Sort": {
		"resource_type": "Image",
		"url": "/build/code/static/img/topo-demo/topo0.png",
		"height": "100%",
		"width": "100%"
	},

	"Kahn's Algorithm Demo": {
		"resource_type": "html",
	   	"url": "/build/code/runtime/html/topo_demo.html"
	},

	"Kahn's Algorithm Practice": {
		"resource_type": "html",
	   	"url": "/build/code/runtime/html/topo_practice.html"
	},

	"Kahn's Algorithm Exercise": {
		"resource_type": "html",
	   	"url": "/build/code/runtime/html/topo_exercise.html"
	},

	// videos 
	"Topological Sort Introduction": {
		"resource_type": "video",
		"url": "https://www.youtube.com/embed/bsg3029xDnw"
	},

	"Video Demo for Topological Sort": {
		"resource_type": "video",
		"url": "https://www.youtube.com/embed/5n1v1iEFFxU"
	},

	"Video Demo for Kahn's Algorithm": {
		"resource_type": "video",
		"url": "https://www.youtube.com/embed/Y5FQYaLFjuE"
	}
};

#+END_SRC

* Tangle
#+BEGIN_SRC javascript :tangle js/realization.js :eval no :noweb yes
<<realization-catalog>>
#+END_SRC
