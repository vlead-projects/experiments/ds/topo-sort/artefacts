#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the common styles which, can be used
  across all the html interactive artefacts.

* Styles
** CSS to set br size same as font-size of text
#+NAME: set-br
#+BEGIN_SRC css
br {
    font-size: 2vw;
}
#+END_SRC

** CSS to set graph at center of iframe.
#+NAME: style-graph
#+BEGIN_SRC css

#graph {
    position: relative;
    padding-top:1vw;
    height: 25vw;
}
#+END_SRC

** CSS to set question just above graph in case of excercises.
#+NAME: style-question
#+BEGIN_SRC css

#que{
  position: relative;
  font-family:arial;
  font-size: 1.5vw;
  padding:1vw;
  }

#+END_SRC

** CSS to set the legend at designated position at the left.
#+NAME: style-legend
#+BEGIN_SRC css

#legend {
  position: absolute;
}
#+END_SRC

** Common CSS to set of the legends .
#+NAME: style-setlegend
#+BEGIN_SRC css

.legend span{
    float: left;
    width: 3.5vw;
    height: 0.5vw;
    margin-right: 0.5vw;
  border-radius: 0%;
  border:0vw;
  margin-top:0.175vw;
  }
#+END_SRC

** CSS of each color of legends .
#+NAME: style-legends
#+BEGIN_SRC css

.legend .lim {background-color: #83F52C;margin-top:0.7vw;height:0.27vw;}  
.legend .blac {background-color: black;margin-top:0.7vw;height:0.2vw;}
.legend .grey {background-color: grey;margin-top:0.7vw;height:0.2vw;}  
.legend .vio {background-color: darkblue;margin-top:0.7vw;height:0.2vw;}  
.legend .blu {background-color: #2C9AD1;margin-top:0.7vw;height:0.27vw;}
.legend .brow{background-color: brown;margin-top:0.7vw;height:0.2vw;}
#+END_SRC


** CSS to position comment box .
#+NAME: style-instructions
#+BEGIN_SRC css


#instruction {
    position: relative;
    margin-top:1vw;
    left: 35%;
    height: 10vw;
    font-family: arial;
}


#instructions {
    font-family: arial;
}
#+END_SRC

** CSS to position the set of buttons at designated place .
#+NAME: style-buttons
#+BEGIN_SRC css


.buttons {
    display: block;
    width: 100%;
    text-align: center;
    position:relative;
    overflow: auto;
    z-index: 1;  
   }
#+END_SRC

** CSS to style a button .
#+NAME: style-button
#+BEGIN_SRC css

  .buttons button{
    z-index: 1;
    width: 15%;
  }
#+END_SRC

** CSS to place slider and text for slider at designated position .
#+NAME: style-ranger
#+BEGIN_SRC css

  #ranger {
    
    position: relative;
    font-size: 1.5vw;
    width:40%;
    z-index: 1;
   }
#+END_SRC

** CSS to place slider and text for slider at designated position .
#+NAME: style-slider
#+BEGIN_SRC css
  .slider{
    width:30%;
   }


#+END_SRC

* Tangle
#+BEGIN_SRC css :tangle mst.css :eval no :noweb yes
<<set-br>>
<<style-graph>>
<<style-question>>
<<style-legend>>
<<style-setlegend>>
<<style-legends>>
<<style-instructions>>
<<style-buttons>>
<<style-button>>
<<style-ranger>>
<<style-slider>>
#+END_SRC
