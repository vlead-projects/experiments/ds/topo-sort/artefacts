#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* Introduction
  This document builds the =Topoligical Sort practice=
  interactive artefact(HTML)


* Features of the artefact
+ Artefact provides a practice module for Topoligical Sort
  Algorithm
+ User can see a randomly generate graph or work on same
  graph as many times as he need.
+ User can click the =New Graph= button to work on a new
  graph at any time.
+ User can click the =Reset= button to work on same graph at
  any time.


* HTML Framework of Topoligical Sort
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Topoligical Sort</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="./../../../docs/static/libs/vis.min.js"></script>
<script src="./../js/Algo.js"></script> 
<script src="./../js/data.js"></script> 
<link href="./../../../docs/static/libs/vis.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
<link href="./../css/mst.css" rel="stylesheet" type="text/css" />
<script src="./../js/topo_practice.js"></script> 
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>

#+END_SRC


** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
  <div class="instruction-box" id="instructions">
    <button class="collapsible"> Instructions </button>
    <div class ="content">
      <ul>
        <li>Select the node that can be added to topologically sorted order.</li>
        <li>Click on the undo to undo the previous step.</li> 
        <li>Click on <b>Reset</b> to start over with a same graph</li>
        <li>Click on <b>New Graph</b> to start over with a new graph</li>
        <li>Zoom or Adjust graph by dragging nodes according to your convience</li>  
      </ul> 
    </div>
  </div>
#+END_SRC
 

*** Graph Holder
This is a graph holder, which contains graph in canvas 
where the graphs are randomly created
#+NAME: graph-holder
#+BEGIN_SRC html
  <div style="font-size: 1.5vw;">
    <p id="seq"><b>Sorted Order : </b></p>
  </div>      
  <center>   
    <div id="graph"></div> 
  </center>
#+END_SRC


*** Controller Elements
This are the controller buttons(Start, Reset, Pause) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: controller-elems
#+BEGIN_SRC html
  <div class="buttons">
    <button id="res" class="button-input">Reset</button>
    <button id="rege" class="button-input">New Graph</button>
  </div>
#+END_SRC


*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
  <div id="instruction"class="comment-box">
    <b>Observations</b><br>
    <p id = "ins"> </p>
  </div>
#+END_SRC


* Tangle
#+BEGIN_SRC html :tangle topo_practice.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
<head>
</head>
<<instruction-box>> 
<<graph-holder>>
<<comments-box>>
<<controller-elems>>
<<head-section-elems>>
</body>
</html>
#+END_SRC



